# Mini-CNC

Documentation repo for the mini-CNC project, replicated during [reGOSH](https://regosh.libres.cc/) Porto Alegre (POA) in 2019.

Development files at the main repo: https://gitlab.com/naikymen-space/hardware/minicnc-regosh-2019

Start here:

- [home.md](./home.md)

Secondary pages:

- [comunicacion.md](./comunicacion.md)
- [relatorio.md](./relatorio.md)
- [sobrehiperobjetos.md](./sobrehiperobjetos.md)
- [teoria.md](./teoria.md)
