# Acercamiento al concepto de HiperObjetos para Tecnologías Libres

El concepto de hiperobjetos lo acuño Rafael Pezzi y es el concepto fundamental de la Bancada de los HiperObjetos, herramientas y máquinas que permiten desarrollar con Open Hardware otras herramientas y desarrollos de investigación.

Básicamente busca promover que las herramientas sean libres tanto de diseño, como estudio y fabricación, tomando de referencia los Hiperlinks como medios de relacionamiento y llevandolo al concepto de HiperObjeto.
Un objeto es HiperObjeto cuando comparte la información para ser creado, para compartir, modificar y materializarlo. 
Los objetos a diferencia del conocimiento sólo tienen una materialidad con un uso limitado y por eso es importante promover sus usos simultaneos promoviendo la difusión del conocimiento de la creación de esos objetos. 


El material puede encontrarse aquí

http://cta.if.ufrgs.br/projects/bancada-dos-hiperobjetos/wiki/Wiki
http://livroaberto.ibict.br/bitstream/1/1060/1/Ciencia%20aberta_questoes%20abertas_PORTUGUES_DIGITAL%20%285%29.pdf

Residencia de CTA, en Porto Alegre, 2019.
