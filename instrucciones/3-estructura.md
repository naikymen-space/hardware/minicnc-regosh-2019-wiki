# Cómo armar la estructura

La estructura final debería verse de la siguente manera:

![Estructura general](3-estructura_soporte.png)

## Materiales
  * Cortadora eléctrica de madera
  * Anteojos de seguridad
  * Orejeras de seguridad
  * Madera
  * Lápiz
  * Escuadra
  * Trincheta
  * Cinta adhesiva
  * Lijadora (opcional)

## 01 - Modelar estrucutra de soporte en FreeCAD
La idea es que haya una base que sostenga un carrito del DVD y columnas que sostengan el otro. Pueden encontrar los planos en el repositorio.

  1. Apoyar un carrito de DVD sobre la madera, y tomar medidas de la altura (~ 3 cm) y del ancho/largo (~ 15x18 cm).
  2. Aproximar la altura a la que debe estar el otro carrito de DVD (~ 18 cm).
  3. Diseñar una base rectangular y dos columnas triangulares para sostener el segundo carrito.
  4. Imprimir los planos a escala (1:1).

Nota: las columnas deben tener un agujero cerca de la base (ver imagen a continuación), para acomodar el primer carrito bajo el segundo. Si las columnas están lo suficientemente separadas, este agujero no es necesario.

![Inline image](3-estructura_columna.png)

## 02 - Cortar la madera para la base
  1. Usar la escuadra y el lápiz para dibujar un rectángulo sobre la madera, de medidas apropiadas (~ 15x18 cm).
  2. Colocarse los elementos de seguridad (anteojos y orejeras).
  3. Cortar la madera sobre una superficie segura.

Para esto usamos una caladora eléctrica pero, por falta de habilidad, no salieron precisas. Finalmente las descartamos y cortamos de nuevo usando una sierra eléctrica.

## 03 - Cortar la madera para las columnas

  1. Pegar el plano de las columnas sobre la madera usando cinta adhesiva.
  2. Pasar la trincheta sobre el perímetro de la figura, con presión suficiente para marcar la madera que está debajo.
  3. Cortar la madera usando las marcas de la trincheta como guía.

Al igual que antes, para cortar derecho es más fácil usar una sierra que una caladora. En primera instancia hicimos las columnas con la caladora, pero finalmente hicimos nuevas columnas con la sierra.

Dos imágenes inspiradoras:

![3-plano-guia](3-plano-guia.jpg)

![corte_columnas](3-corte_columnas.jpg)

## 04 - Pulir imperfecciones (opcional)

Recomendamos cortar las piezas lo mejor posible para evitar este paso.

Nuestras piezas de madera no fueron cortadas perfectamente, por lo que usamos una lijadora eléctrica para enderezar los bordes. Sin embargo, las imperfecciones trajeron problemas más adelante.

## 05 - Agujerear base y columnas

Los agujeros en cada columna tienen que respetar las distancias entre los agujeros de montaje en el carrito del DVD. Hicimos las marcas acomodando todo primero.

![3-marcas-columnas](3-marcas-columnas.jpg)

Nivelar agujeros para las columnas antes de perforar:

![3-nivel-columna](3-nivel-columna.jpg)

Insertar varilla roscada:

![3-agujero-columna-y-rosca](3-agujero-columna-y-rosca.jpg)

La base fue agujereada aproximadamente, para acomodar 4 clavos (dos por cada columna).

Una vez colocadas las varillas roscadas en las columnas

## 06 - Ensamblar base, columnas y carritos de DVD

Usamos reglas y escuadras para marcar la posición correcta de cada parte, asegurando que luego los carritos queden en orientaciones perpendiculares (el de abajo se meuve en dirección Y, y el de arriba en direccion X).

![3-union-base-columna](3-union-base-columna.jpg)

![3-ensamblado-carritos](3-ensamblado-carritos.jpg)

## 07 - Soporte para el papel

Un cachito de madera con cinta doble faz encima, y por encima un cuadrado de PCB :)

![3-base-papel](3-base-papel.jpg)

![3-base-papel2](3-base-papel2.jpg)

## 08 - Soporte para la lapicera y servo

Este también fue un bloque de madera atornillado al carrito de DVD.

Hicimos agujeros en la madera para que la lapicera pudiera desplazarse en la dirección correcta.

Usamos un bloque de sacrificio para poder agujerear la madera de forma que quede una canaleta.

![3-taco-lapicera1](3-taco-lapicera1.jpg)

![3-taco-lapicera1](3-taco-lapicera2.jpg)

También para acomodar el servo en una posición útil.

![3-taco-servo1](3-taco-servo0.jpg)

![3-taco-servo1](3-taco-servo1.jpg)

Así quedó:

![3-taco-servo1](3-taco-servo2.jpg)

## 09 - Tornillo para actuador servo-lapicera

Finalmente colocamos un tornillo a través de la cánula de la lapicera, que quedara por debajo del brazo del servo.

También usamos bandas elásticas para mantener la lapicera levantada, hasta que el servo bajara su brazo.

- [ ] Sry no pics :(
